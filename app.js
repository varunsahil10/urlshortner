var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ejs = require('ejs');

var url = 'mongodb://localhost:27017/shorturl';
var dbname = 'shorturl';
var appUrl = "localhost:3000/";

app.set('view engine','ejs');
app.use(express.static(__dirname + '/public'));

MongoClient.connect(url, (err, client) => {

    assert.equal(err,null);
    console.log('Connected correctly to server');
    var db = client.db(dbname);
    var collection = db.collection("urls"); 

    app.get('/new/:longUrl(*)',(req,res)=>{
        var longUrl = req.params.longUrl;
        var uniqueId = new Date().getTime();
        uniqueId = uniqueId.toString().slice(0,-2);
        var shorturl = appUrl+uniqueId;

    
    collection.insert({
        "longUrl": longUrl,
        "shorturl": shorturl,
        "uniqueId": uniqueId
        },
    (err, result) => {
        assert.equal(err,null);
        console.log("After Insert:\n");
        console.log(result.ops);
        var l = result.ops[0].longUrl;
        var s = result.ops[0].shorturl;
        var obj = {
            "original": l,
            "short": s
        };
        res.send(obj);
    });
 });

    app.get("/:uniqueId",(req,res)=>{
        var uniqueId = req.params.uniqueId;

        collection.find({
            "uniqueId": uniqueId
        }).toArray((err,data)=>{
            if (err) {return console.log("error")}
            if (data.length == 0) {return console.log("no match found")}
                var final = data[0].longUrl;
            res.redirect(final);
        });
    });
// client.close();
});//end db

app.get('/',(req,res)=>{
    res.status(200).render('index');
});

app.listen(3000,
console.log("app on port 3000")
    );